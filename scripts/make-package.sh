#!/bin/bash

fpm -s dir -t deb -n php-example-project \
      --prefix=/var/www/php-example-project/ \
      --directories=. \
      --exclude='.git' \
      --exclude='.gitlab-ci.yml' \
      --exclude='.gitignore' \
      --exclude='scripts' \
      --exclude='**/.git' \
      --exclude='Tests' \
      --exclude='phpunit*' \
      --exclude='composer.json' \
      --exclude='composer.lock' \
      --deb-user 10001 \
      --deb-group 15000 \
      --template-scripts \
      --iteration=`git rev-parse HEAD` \
      --version=`date +%s` \
      .

